
import sys
import random
from . import board_properties as PropertiesLib


class InvalidBoardPropertyError(Exception):
    def __init__(self, message):
        super(InvalidBoardPropertyError, self).__init__(message)
        self.message = message
        self.case = "The property " + str(message) + " was not found."

    def __str__(self):
        sys.stderr.write(self.case)


class InvalidPositionError(Exception):
    def __init__(self, message):
        super(InvalidPositionError, self).__init__(message)
        self.message = message
        self.case = "The position " + str(message) + " is invalid."

    def __str__(self):
        sys.stderr.write(self.case)


class BoardError(Exception):
    def __init__(self, message):
        super(BoardError, self).__init__(message)
        self.message = message
        self.case = "Not a valid board operation. " + str(self.message)

    def __str__(self):
        sys.stderr.write(self.case)


class MonopolyBoard():
    def __init__(self):
        self.players = []
        self.properties = PropertiesLib.BoardProperties()
        self.current_dice = 0
        self.player_turn_id = -1

        # self.landing_chances ... (property_index, player_1_chance, player_2_chance, etc)

    def InitPlayers(self, num_players):
        for i in range(num_players):
            self.players.append(Player(i))
        self.player_turn_id = 0

    @staticmethod
    def PropertyNameExists(property_name):
        if property_name in [k[1]["name"] for k in PropertiesLib.BoardProperties().tuples]:
            return True
        else:
            for i in PropertiesLib.BoardProperties().tuples:
                if PropertiesLib.BoardProperties.UnicodeAsciiCompare(unicode_string = i[1]["name"], ascii_string = property_name):
                    return True
        return False

    @staticmethod
    def PropertyIdExists(property_id):
        if property_id in [k[0] for k in PropertiesLib.BoardProperties().tuples]:
            return True
        return False

    @staticmethod
    def GetPropertyIdAtPosition(position_x):
        for i in PropertiesLib.BoardProperties().tuples:
            if int(i[1]['board_position']) == int(position_x):
                return i
        return None

    @staticmethod
    def GetPropertyWithName(name):
        for i in PropertiesLib.BoardProperties().tuples:
            if PropertiesLib.BoardProperties.UnicodeAsciiCompare(ascii_string = str(name), unicode_string = i[1]['name']):
                return i
        return None

    def GetAllPropertiesNames(self):
        return [t[1]['name'] for t in PropertiesLib.BoardProperties().tuples]

    def PropertyAtPositionIsOwnedByOtherPlayer(self, position_x, original_player):
        property = MonopolyBoard.GetPropertyIdAtPosition(position_x)
        if property is None:
            return False
        id = property[0]

        for player_id in range(len(self.players)):
            if player_id == original_player:
                continue
            for property_id in self.players[player_id].properties:
                # print(property_id)
                if id == property_id:
                    print("owned")
                    return True

        return False

    def DiceSimulation(self, player_id = -1, set_dice=True, manual_dice = None):
        if player_id == -1:
            raise BoardError("Player does not exist.")
        if player_id < 0 or player_id > len(self.players) - 1:
            raise BoardError("Player id out of bounds.")

        probability = [0, 0, 2.78, 5.56, 8.33, 11.11, 13.89,
                        16.67, 13.89, 11.11, 8.33, 5.56, 2.78
        ]

        landing_probabilities = [
            (self.players[player_id].posX + k, probability[k])
            for k in range(2, 12 + 1)
        ]

        # resolve positions
        for k in range(len(landing_probabilities)):
            if landing_probabilities[k][0] > 39:
                landing_probabilities[k] = (40 - landing_probabilities[k][0], landing_probabilities[k][1])
        # end resolve positions

        return_dice = 0
        if not manual_dice:
            random_dice = int(random.randrange(2, 12))
        else:
            random_dice = manual_dice
        return_dice = random_dice # blunder

        print("generating random dice")
        self.current_dice = return_dice
        if set_dice:
            if self.player_turn_id == len(self.players) - 1:
                self.player_turn_id = 0
            else:
                self.player_turn_id += 1
        print(return_dice)
        return return_dice, landing_probabilities

    def WorstCase(self, player_id = -1):
        current_dice, probs = self.DiceSimulation(player_id = player_id)
        MonopolyBoard.GetPropertyIdAtPosition(probs[0][0])

        costs = []

        for p in probs:
            position = p[0]
            prob = p[1]
            property = MonopolyBoard.GetPropertyIdAtPosition(position)
            cost = 0 if property is None else int(property[1]['value'])
            invest = 0

            if self.PropertyAtPositionIsOwnedByOtherPlayer(position, player_id):
                cost *= -1
            else:
                invest = cost # TODO: houses
                cost = 0

            costs.append((position, prob, cost, invest))

        return costs

    def UpdatePlayerProbabilities(self):
        for player in self.players:
            simulations = self.DiceSimulation(player.id, set_dice = False)[1]
            ppp_list = [] # (player, property, landing_probability)
            player = self.players[player.id]

            print("dbg simulations")
            print(simulations)
            # simulations is a dice_roll -> probability tuple list
            for item in simulations:
                property_id = None # a function of position
                landing_probability = item[1] # directly taken from simulations

                position_of_dice = item[0] + player.posX
                # now it's possible to infer property_name_at_position, property_id_at_position
                # find out if is there a property even at the position?
                prop_id_rval = MonopolyBoard.GetPropertyIdAtPosition(position_of_dice)
                if prop_id_rval == None:
                    continue
                else:
                    property_id = prop_id_rval[0]

                ppp_tuple = (player.id, position_of_dice, item[1], property_id)
                ppp_list.append(ppp_tuple)

            print("dbg ppp list")
            print(ppp_list)
            player.UpdateLandingChances(ppp_list) # ... but check for property existence
            # all we need is player-probability-property tuples for each property

        #raise NotImplementedError("working on it")

    def PerformTransaction(self, value, source_player_id, destination_player_id):
        self.players[source_player_id].cash -= value
        self.players[destination_player_id].cash += value
        print("tried from {0} to {1}".format(source_player_id, destination_player_id))

    def PerformTransactionRent(self, p1, position):
        # 1. get property at position
        query_result = MonopolyBoard.GetPropertyIdAtPosition(position)
        if query_result == None:
            return False

        # 2. get its value
        value = int(query_result[1]['value'])

        # 3. get owner
        for player in self.players:
            for property in player.properties:
                if property[0] == query_result[0]:
                    # 4. transact its value from p1 to owner
                    self.PerformTransaction(value=value, source_player_id=p1, destination_player_id=player.id)
                    return True

    def PerformRoll(self, manual_dice = None, disable_auto_transaction=False):
        """
            This no argument method should end the current player's turn
        """
        print("Debugging player turn id.")
        print(self.player_turn_id)
        return_dice = self.DiceSimulation(self.player_turn_id, set_dice = True, manual_dice = manual_dice)[0]
        self.UpdatePlayerProbabilities()
        print(self.players[self.player_turn_id].landing_chances_properties)
        print("return dice: {0}".format(return_dice))

        print(self.players[1])
        print("posx before updating {0}".format(self.players[self.player_turn_id].posX))
        self.players[self.player_turn_id].SetPosX(self.players[self.player_turn_id].posX + return_dice)
        if not disable_auto_transaction:
            self.PerformTransactionRent(p1=self.player_turn_id, position=self.players[self.player_turn_id].posX + return_dice)
        print("posx after updating {0}".format(self.players[self.player_turn_id].posX))


        #region 1
        print("changepos_x")

        print("players")
        print(len(self.players))
        #self.player_turn_id = self.player_turn_id + 1 if self.player_turn_id + 1 < len(self.players) else 0
        print(self.player_turn_id)
        #self.player_turn_id += 1
        print(self.player_turn_id)
        #if self.player_turn_id == len(self.players):
        #    print("CASE RESET ID")
        #           self.player_turn_id = 0
        print("player turn id:")
        print(self.player_turn_id)
        #endregion

class Player():
    def __init__(self, id):
        self.id = id
        self.posX = 0
        self.cash = 0
        self.properties = []
        self.landing_chances_properties = [] # a simple tuple list of player-probability-property
        # self.landing_chances_properties ... (property_id, landing_chance)

    def UpdateLandingChances(self, ppp_list):
        """
            This ought to SET the self.landing_chances_properties
            Using the board's Simulation (but without setting dice)
        """
        self.landing_chances_properties = ppp_list

    def SetPosX(self, new_value):
        if new_value < 0:
            raise InvalidPositionError(new_value)
        print("NOP")
        print(new_value)
        if new_value >= 40:
            new_value = new_value - 40
        self.posX = new_value

    def RemoveProperty(self, property_template):
        if type(property_template) is str:
            if not MonopolyBoard.PropertyNameExists(property_template):
                raise InvalidBoardPropertyError(property_template)
            if MonopolyBoard.GetPropertyWithName(property_template) is None:
                raise InvalidBoardPropertyError("Not a valid name.")
            propertyOBJ = MonopolyBoard.GetPropertyWithName(property_template)
            propertyID = propertyOBJ[0]

            found_pos = None
            ct = 0
            for obj in self.properties:
                pr_name = obj[1]
                if pr_name == propertyOBJ[1]['name']:
                    found_pos = ct
                    break
                ct += 1
            self.properties.pop(ct)

            #self.properties.append((propertyID, propertyOBJ[1]['name']))
        elif type(property_template) is int:
            if not MonopolyBoard.PropertyIdExists(property_template):
                raise InvalidBoardPropertyError(property_template)
            if MonopolyBoard.GetPropertyIdAtPosition(property_template) is None:
                raise InvalidBoardPropertyError(str(property_template))
            propertyOBJ = MonopolyBoard.GetPropertyIdAtPosition(property_template)
            propertyID = propertyOBJ[0]
            #self.properties.append((propertyID, propertyOBJ[1]['name']))

            found_pos = None
            ct = 0
            for obj in self.properties:
                pid = obj[0]
                if pid == propertyID:
                    found_pos = ct
                    break
                ct += 1
            self.properties.pop(ct)

        else:
            raise InvalidBoardPropertyError(property_template)

    def AddProperty(self, property_template):
        if type(property_template) is str:
            if not MonopolyBoard.PropertyNameExists(property_template):
                raise InvalidBoardPropertyError(property_template)
            if MonopolyBoard.GetPropertyWithName(property_template) is None:
                raise InvalidBoardPropertyError("Not a valid name.")
            propertyOBJ = MonopolyBoard.GetPropertyWithName(property_template)
            propertyID = propertyOBJ[0]
            self.properties.append((propertyID, propertyOBJ[1]['name']))
        elif type(property_template) is int:
            if not MonopolyBoard.PropertyIdExists(property_template):
                raise InvalidBoardPropertyError(property_template)
            if MonopolyBoard.GetPropertyIdAtPosition(property_template) is None:
                raise InvalidBoardPropertyError(str(property_template))
            propertyOBJ = MonopolyBoard.GetPropertyIdAtPosition(property_template)
            propertyID = propertyOBJ[0]
            self.properties.append((propertyID, propertyOBJ[1]['name']))
        else:
            raise InvalidBoardPropertyError(property_template)
