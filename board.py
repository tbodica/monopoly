
import sys
import random
import board_properties as PropertiesLib

class InvalidBoardPropertyError(Exception):
    def __init__(self, message):
        super(InvalidBoardPropertyError, self).__init__(message)
        self.message = message
        self.case = "The property " + str(message) + " was not found."

    def __str__(self):
        sys.stderr.write(self.case)
        # try:
        #     sys.stderr.write(self.case)
        #     print("OK")
        # except Exception as ex:
        #     raise ex
        #     print(ex)


class InvalidPositionError(Exception):
    def __init__(self, message):
        super(InvalidPositionError, self).__init__(message)
        self.message = message
        self.case = "The position " + str(message) + " is invalid."

    def __str__(self):
        sys.stderr.write(self.case)

class BoardError(Exception):
    def __init__(self, message):
        super(BoardError, self).__init__(message)
        self.message = message
        self.case = "Not a valid board operation. " + str(self.message)

    def __str__(self):
        sys.stderr.write(self.case)

class MonopolyBoard():
    def __init__(self):
        self.players = []
        self.properties = PropertiesLib.BoardProperties()
        self.current_dice = 0

    def InitPlayers(self, num_players):
        for i in range(num_players):
            self.players.append(Player())

    @staticmethod
    def PropertyNameExists(property_name):
        if property_name in [k[1]["name"] for k in PropertiesLib.BoardProperties().tuples]:
            return True
        else:
            for i in PropertiesLib.BoardProperties().tuples:
                if PropertiesLib.BoardProperties.UnicodeAsciiCompare(unicode_string = i[1]["name"], ascii_string = property_name):
                    return True
        return False

    @staticmethod
    def PropertyIdExists(property_id):
        if property_id in [k[0] for k in PropertiesLib.BoardProperties().tuples]:
            return True
        return False

    @staticmethod
    def GetPropertyIdAtPosition(position_x):
        for i in PropertiesLib.BoardProperties().tuples:
            if int(i[1]['board_position']) == int(position_x):
                return i
        return None

    @staticmethod
    def GetPropertyWithName(name):
        for i in PropertiesLib.BoardProperties().tuples:
            # print("comparing ")
            # print(str(name))
            # print(i[1]['name'])
            if PropertiesLib.BoardProperties.UnicodeAsciiCompare(ascii_string = str(name), unicode_string = i[1]['name']):
                return i
        return None

    def PropertyAtPositionIsOwnedByOtherPlayer(self, position_x, original_player):
        property = MonopolyBoard.GetPropertyIdAtPosition(position_x)
        if property is None:
            return False
        id = property[0]

        for player_id in range(len(self.players)):
            if player_id == original_player:
                continue
            for property_id in self.players[player_id].properties:
                # print(property_id)
                if id == property_id:
                    print("owned")
                    return True

        return False



    def DiceSimulation(self, player_id = -1):
        if player_id == -1:
            raise BoardError("Player does not exist.")
        if player_id < 0 or player_id > len(self.players) - 1:
            raise BoardError("Player id out of bounds.")

        probability = [0, 0, 2.78, 5.56, 8.33, 11.11, 13.89,
                        16.67, 13.89, 11.11, 8.33, 5.56, 2.78
        ]

        landing_probabilities = [
            (self.players[player_id].posX + k, probability[k])
            for k in range(2, 12 + 1)
        ]

        # resolve positions
        for k in range(len(landing_probabilities)):
            if landing_probabilities[k][0] > 39:
                landing_probabilities[k][0] = 40 - landing_probabilities[k][0]
        # end resolve positions
        # print(landing_probabilities)

        return_dice = 0
        random_dice = int(random.randrange(2, 12))

        self.current_dice = return_dice
        return return_dice, landing_probabilities

    def WorstCase(self, player_id = -1):
        current_dice, probs = self.DiceSimulation(player_id = player_id)
        MonopolyBoard.GetPropertyIdAtPosition(probs[0][0])

        costs = []

        for p in probs:
            # print(p)
            position = p[0]
            prob = p[1]
            # costs.append((position, prob, MonopolyBoard.GetPropertyIdAtPosition(position)["value"]))
            property = MonopolyBoard.GetPropertyIdAtPosition(position)
            # print(property)
            cost = 0 if property is None else int(property[1]['value'])
            invest = 0

            if self.PropertyAtPositionIsOwnedByOtherPlayer(position, player_id):
                cost *= -1
            else:
                invest = cost # TODO: houses
                cost = 0

            costs.append((position, prob, cost, invest))

        return costs


class Player():
    def __init__(self):
        self.posX = 0
        self.cash = 0
        self.properties = []

    def SetPosX(self, new_value):
        if new_value > 39 or new_value < 0:
            raise InvalidPositionError(new_value)

    def AddProperty(self, property_template):
        if type(property_template) is str:
            if not MonopolyBoard.PropertyNameExists(property_template):
                raise InvalidBoardPropertyError(property_template)
            if MonopolyBoard.GetPropertyWithName(property_template) is None:
                raise InvalidBoardPropertyError("Not a valid name.")
            self.properties.append(MonopolyBoard.GetPropertyWithName(property_template)[0])
        elif type(property_template) is int:
            if not MonopolyBoard.PropertyIdExists(property_template):
                raise InvalidBoardPropertyError(property_template)
            if MonopolyBoard.GetPropertyIdAtPosition(property_template) is None:
                raise InvalidBoardPropertyError(str(property_template))
            self.properties.append(MonopolyBoard.GetPropertyIdAtPosition(property_template)[0])
        else:
            raise InvalidBoardPropertyError(property_template)
