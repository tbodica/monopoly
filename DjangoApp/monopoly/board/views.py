from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from .MonopolyBoard.board import *
from django import forms

class ResetBoardForm(forms.Form):
    reset = forms.CharField()

class RollForm(forms.Form):
    CHOICES= (
        ('AutoOn','Enabled'),
        ('AutoOff','Disabled'),
    )
    auto_mode = forms.ChoiceField(choices=CHOICES)
    roll = forms.CharField()

class ManualDice(forms.Form):
    dice_value = forms.CharField()


class ManualTransaction(forms.Form):
    sending_player_id = forms.CharField()
    receiving_player_id = forms.CharField()
    transaction_value = forms.CharField()

class ManualPropertyChange(forms.Form):
    player_id = forms.CharField()
    prop_name = forms.CharField()

    CHOICES= (
        ('Add','Add'),
        ('Remove','Remove'),
    )
    operator = forms.ChoiceField(choices=CHOICES)

def index(request):
    chc_context = None
    if request.method == 'POST':
        reset_form = ResetBoardForm(request.POST)
        if reset_form.is_valid():
            print("VALID FORM") # debug
            val = reset_form.cleaned_data.get("reset")
            request.session['main_board'] = None

        roll_form = RollForm(request.POST)
        if roll_form.is_valid():
            print("VALID ROLL FORM") # debug
            val = roll_form.cleaned_data.get("roll") # ???
            chc = roll_form.cleaned_data["auto_mode"]
            print("chc")
            print(chc)
            chc_context = chc
            print("PERFORM ACTION ROLL")
            if request.session['main_board'] == None:
                raise NotImplementedError("Board not initalized")
            boardcopy = request.session['main_board']
            if chc == 'AutoOn':
                boardcopy.PerformRoll()
            else:
                boardcopy.PerformRoll(disable_auto_transaction=True)
            request.session['main_board'] = boardcopy

        manual_dice = ManualDice(request.POST)
        if manual_dice.is_valid():
            val = manual_dice.cleaned_data.get('dice_value')
            int_val = int(val)
            boardcopy = request.session['main_board']
            boardcopy.PerformRoll(manual_dice = int_val)
            request.session['main_board'] = boardcopy

        manual_transaction = ManualTransaction(request.POST)
        if manual_transaction.is_valid():
            p1 = manual_transaction.cleaned_data.get('sending_player_id')
            p2 = manual_transaction.cleaned_data.get('receiving_player_id')
            tv = manual_transaction.cleaned_data.get('transaction_value')

            try:
                p1 = int(p1)
                p2 = int(p2)
                tv = int(tv)
            except:
                return HttpResponse("Player id INT")

            boardcopy = request.session['main_board']
            try:
                boardcopy.PerformTransaction(value=tv, source_player_id=p1, destination_player_id=p2)
            except:
                return HttpResponse("Problem.")
            request.session['main_board'] = boardcopy

        manual_property_change = ManualPropertyChange(request.POST)
        if manual_property_change.is_valid():
            p1_id = int(manual_property_change.cleaned_data.get('player_id'))
            pr_name = manual_property_change.cleaned_data.get('prop_name')
            option = manual_property_change.cleaned_data['operator'] # Add/Rem
            # TODO p1_id INT
            boardcopy = request.session['main_board']
            if option == "Add":
                boardcopy.players[p1_id].AddProperty(pr_name) # TODO CONTEXT ERROR MSG
            elif option == "Remove":
                boardcopy.players[p1_id].RemoveProperty(pr_name) # always remove by name
                #raise NotImplementedError("NIE") # TODO: remove property
            request.session['main_board'] = boardcopy

    if not request.session.get('main_board'):
        request.session['main_board'] = MonopolyBoard()
        request.session['main_board'].InitPlayers(2)
        request.session['main_board'].players[0].cash = 10
        request.session['main_board'].players[1].posX = 0
        request.session['main_board'].players[0].AddProperty(3)
        request.session['main_board'].players[1].AddProperty("Rahova")
        request.session['main_board'].players[1].AddProperty("Titan")
        request.session['main_board'].players[1].AddProperty("Cotroceni")
        request.session['main_board'].players[1].AddProperty("Piata Unirii")

    print(list(request.session.get('main_board').GetAllPropertiesNames()))
    template = loader.get_template('board/index.html')
    # TODO CONTEXT ERROR MSG
    context = {
        'board_player_turn': int(request.session.get('main_board').player_turn_id),
        'players': list(request.session.get('main_board').players),
        'properties': list(request.session.get('main_board').GetAllPropertiesNames()),
        'auto_mode_def': str(chc_context)
    }
    return HttpResponse(template.render(context, request))

def about(request):
    return HttpResponse("Something")
