import json
import pathlib

class BoardProperties():
    def __init__(self):
        self.tuples = []
        fn = pathlib.Path(__file__).parent / 'Resources/property_list.json'
        with open(fn, 'r') as j:
            data = j.read()
            json_struct = json.loads(data)["properties"]

            list_of_properties = [i for i in json_struct]
            for i, v in enumerate(list_of_properties):
                self.tuples.append((i, v))

    @staticmethod
    def UnicodeAsciiCompare(unicode_string, ascii_string):
        if unicode_string == ascii_string:
            return True # assertion
        hex = ""
        for k in unicode_string:
            if k.lower() == 'ă':
                hex += 'a'
            elif k.lower() == 'â':
                hex += 'a'
            elif k.lower() == 'î':
                hex += 'i'
            elif k.lower() == 'ș':
                hex += 's'
            elif k.lower() == 'ț':
                hex += 't'
            else:
                hex += k

        if hex.lower() == ascii_string.lower():
            return True
        return False
