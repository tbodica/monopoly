


import board as BoardLib



def run_all_tests():
    board = BoardLib.MonopolyBoard()
    board.InitPlayers(2)
    board.players[0].cash = 1000
    board.players[1].cash = 120

    board.players[0].AddProperty(1)
    try:
        board.players[0].AddProperty(2)
        assert False # position 2 is special property
    except:
        assert True

    board.players[0].AddProperty("Giulesti")
    board.players[0].AddProperty("Bdul Brașov")

    board.players[1].AddProperty(8)

    try:
        board.players[0].AddProperty("Giuulesti")
        assert False
    except BoardLib.InvalidBoardPropertyError as ipe:
        assert True

    # add unittestpackage
    try:
        board.players[0].SetPosX(50)
        assert False
    except BoardLib.InvalidPositionError as ipe:
        assert True

    # add unittestpackage
    try:
        board.players[1].SetPosX(8)
        assert True
    except BoardLib.InvalidPositionError as ipe:
        assert False

    return_dice = board.DiceSimulation(player_id = 1)[0]
    try:
        return_dice = board.DiceSimulation(player_id = 2)[0]
        assert False
    except BoardLib.BoardError:
        assert True

    landing_probabilities = board.DiceSimulation(player_id = 1)[1] # player 1 dice
    landing_probabilities.sort(reverse=True)

    assert type(landing_probabilities[0][0]) is int
    assert isinstance(landing_probabilities[0][1], float)
    assert len(landing_probabilities) == 11

    assert return_dice == board.current_dice

    cost = board.WorstCase(player_id = 0)
    assert type(cost) is list
    # print(cost)

    board.players[1].SetPosX(20)
    cost = board.WorstCase(player_id = 1)
    print(cost)

run_all_tests()
